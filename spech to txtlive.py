import sounddevice as sd
from scipy.io.wavfile import write
import numpy as np
import speech_recognition as sr
import openai
from gtts import gTTS
import playsound
import io

def record_audio(duration=5, fs=44100, channels=1):
    print("Recording for {} seconds...".format(duration))
    try:
        recording = sd.rec(int(duration * fs), samplerate=fs, channels=channels, dtype='float64')
        sd.wait()
        return recording
    except Exception as e:
        print("An error occurred while recording audio:", e)
        return None

def save_audio_file(recording, fs=44100, filename='temp.wav'):
    if recording is not None:
        write(filename, fs, recording)

def recognize_speech_from_file(filename='temp.wav'):
    recognizer = sr.Recognizer()
    with sr.AudioFile(filename) as source:
        audio_data = recognizer.record(source)
    try:
        text = recognizer.recognize_google(audio_data)
        print("Transcription: " + text)
        return text
    except sr.UnknownValueError:
        print("Google Speech Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Speech Recognition service; {0}".format(e))

def get_openai_response(text):
    openai.api_key = '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'
    try:
        response = openai.Completion.create(
          engine="text-davinci-004",
          prompt=text,
          max_tokens=150
        )
        return response.choices[0].text.strip()
    except Exception as e:
        print("Error in getting response from OpenAI:", e)
        return None

def text_to_speech(text, filename='response.mp3'):
    tts = gTTS(text=text, lang='en')
    tts.save(filename)
    playsound.playsound(filename)

def main():
    fs = 44100  # Sample rate
    duration = 5  # Duration of recording in seconds

    print("Please speak...")
    recording = record_audio(duration, fs)
    if recording is not None:
        save_audio_file(recording, fs)
        
        spoken_text = recognize_speech_from_file()
        if spoken_text:
            response = get_openai_response(spoken_text)
            if response:
                print("AI Response:", response)
                text_to_speech(response)

if __name__ == "__main__":
    main()
